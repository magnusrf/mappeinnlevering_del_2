package student.ntnu.idatt2001.magnusfarstad.model;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import student.ntnu.idatt2001.magnusfarstad.exception.AddException;
import student.ntnu.idatt2001.magnusfarstad.exception.RemoveException;
import student.ntnu.idatt2001.magnusfarstad.model.PatientRegister;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Patient register test class")
public class PatientRegisterTest {

    @DisplayName("Tests for addPatient()")
    @Nested
    public class addPatientTest {

        @DisplayName("Successfully add a patient to the register")
        @Test
        public void successfullyAddPatient() {
            PatientRegister patientRegister = new PatientRegister();
            boolean success = false;
            try {
                Patient patient = new Patient
                        ("12345678909", "Jon",
                                "Fiskerud", "Bad neck",
                                "Kristian Jansen");
                patientRegister.addPatient(patient);
            } catch (AddException | Exception exception) {
                System.out.println(exception.getMessage());
            } finally {
                if(patientRegister.getPatients().get(0).getSocialSecurityNumber()
                        .equals("12345678909")) {
                    success = true;
                }
                assertTrue(success);
            }
        }

        @DisplayName("Successfully handle adding existing patient to the register")
        @Test
        public void unsuccessfullyAddPatient() {
            PatientRegister patientRegister = new PatientRegister();
            try {
                Patient patient = new Patient
                        ("12345678909", "Jon",
                                "Fiskerud", "Bad neck",
                                "Kristian Jansen");
                patientRegister.addPatient(patient);
                patientRegister.addPatient(patient);
                fail("Method addPatient() did not throw Exception as expected");
            } catch (Exception | AddException exception) {
                assertEquals("This patient already exists", exception.getMessage());
            }
        }
    }

    @DisplayName("Tests for deletePatient()")
    @Nested
    public class deletePatient {

        @Test
        public void successfullyDeletePatient() {
            PatientRegister patientRegister = new PatientRegister();
            try {
                Patient patient = new Patient
                        ("12345678909", "Jon",
                                "Fiskerud", "Bad neck",
                                "Kristian Jansen");
                patientRegister.addPatient(patient);
                patientRegister.deletePatient(patient);

                assertTrue(patientRegister.getPatients().stream()
                        .noneMatch(p -> p.equals(patient)));
            } catch (Exception | RemoveException | AddException exception) {
                System.out.println(exception.getMessage());
                fail("Method deletePatient() threw unexpected RemoveException");
            }
        }

        @Test
        public void successfullyHandlesNullInput() {
            PatientRegister patientRegister = new PatientRegister();
            try {
                patientRegister.deletePatient(null);
                fail("Method deletePatient() did not throw RemoveException as expected");
            } catch (RemoveException exception) {
                assertEquals("Patient not found", exception.getMessage());
            }
        }
    }
}
