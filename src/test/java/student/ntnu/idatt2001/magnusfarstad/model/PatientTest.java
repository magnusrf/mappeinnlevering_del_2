package student.ntnu.idatt2001.magnusfarstad.model;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Patient test class")
public class PatientTest {

    @DisplayName("Successfully create a patient")
    @Test
    public void successfullyCreatesPatient() {
        boolean success = true;
        try {
            Patient patient = new Patient("73950482716", "Kristian", "Grannesen",
                    "Cold", "Dr. Bergersen");
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            success = false;
        } finally {
            assertTrue(success);
        }
    }

    @DisplayName("Successfully handle illegal input")
    @Nested
    public class successfullyHandlesIllegalInput {

        @DisplayName("Successfully handle null input")
        @Test
        public void successfullyHandlesNullInput() {
            try {
                Patient patient = new Patient("", "", "",
                        "", "");
                fail("Constructor did not throw Exception as expected");
            } catch (Exception exception) {
                assertEquals(
                        "Person must have first name, last name and social security number",
                        exception.getMessage());
            }
        }

        @DisplayName("Successfully handle wrong social security number input")
        @Test
        public void successfullyHandlesWrongSocialSecurityNumber() {
            try {
                Patient patient = new Patient("1234567", "Jan", "Banan",
                        "Legally blind", "Dr. Proktor");
                fail("Constructor did not throw Exception as expected");
            } catch (Exception exception) {
                assertEquals("Social security number must be of length 11, numbers only",
                        exception.getMessage());
            }
        }

        @DisplayName("Successfully handle number input in names")
        @Test
        public void successfullyHandlesNumberInputInNames() {
            try {
                Patient patient = new Patient("12345678909", "123", "Petter",
                        "Ill", "Dr. Gran");
                fail("Constructor did not throw Exception as expected");
            } catch (Exception exception) {
                assertEquals("First name, last name and general practitioner " +
                        "must be text, not numbers", exception.getMessage());
            }
        }
    }
}
