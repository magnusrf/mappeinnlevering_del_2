package student.ntnu.idatt2001.magnusfarstad.filehandling;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import student.ntnu.idatt2001.magnusfarstad.model.PatientRegister;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class CSVReaderTest {

    @Test
    public void successfullyReadFromCSVFile() {
        CSVReader reader = new CSVReader();
        String path = "/Users/Farstad/OneDrive/Mappeinnlevering_Del_2/src/test/resources/testfiles/TestfileOne.csv";
        File file = new File(path);
        PatientRegister patients = new PatientRegister();

        try {
            patients = reader.readToCSV(patients, file);
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        } finally {
            assertEquals("12345678909", patients.getPatients().get(0).getSocialSecurityNumber());
        }
    }

    @Nested
    public class successfullyHandlesExceptions {

        @Test
        public void successfullyHandlesWrongPath() {
            CSVReader reader = new CSVReader();
            String path = "/Users/Farstad/NothingHere.csv";
            File file = new File(path);
            PatientRegister patients = new PatientRegister();
            String message = "";

            try {
                patients = reader.readToCSV(patients, file);
                fail("The method read() threw unexpected IOException");
            } catch (IOException exception) {
                message = exception.getMessage();

            } finally {
                assertEquals("The file " + file.getName() + " is not found or does not exist",
                        message);
            }
        }

        @Test
        public void successfullyHandlesNotCSVFile() {
            CSVReader reader = new CSVReader();
            String path = "/Users/Farstad/OneDrive/Mappeinnlevering_Del_2/src/main/resources/student/ntnu/idatt2001/magnusfarstad/images/addNew.png";
            File file = new File(path);
            PatientRegister patients = new PatientRegister();
            String message = "";

            try {
                patients = reader.readToCSV(patients, file);
                fail("Method read() threw unexpected IOException");
            } catch (IOException exception) {
                message = exception.getMessage();

            } finally {
                assertEquals("The chosen file type is not valid. " +
                                "Please chose another file by clicking on 'Import from .CSV' or " +
                                "cancel the operation",
                        message);
            }
        }

        @Test
        public void successfullyHandlesNullInput() {
            CSVReader reader = new CSVReader();
            PatientRegister patients = new PatientRegister();
            String message = "";

            try {
                patients = reader.readToCSV(patients, null);
                fail("Method read() threw unexpected IOException");
            } catch (IOException exception) {
                message = exception.getMessage();
            } finally {
                assertEquals("No file imported", message);
            }
        }
    }

}
