package student.ntnu.idatt2001.magnusfarstad.filehandling;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import student.ntnu.idatt2001.magnusfarstad.exception.AddException;
import student.ntnu.idatt2001.magnusfarstad.model.Patient;
import student.ntnu.idatt2001.magnusfarstad.model.PatientRegister;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

@DisplayName("CSV Writer test class")
public class CSVWriterTest {

    @DisplayName("Successfully writes to an existing file")
    @Test
    public void successfullyWritesToAnExistingFile() {
        CSVWriter writer = new CSVWriter();
        String message = "";
        String path = "/Users/Farstad/OneDrive/Mappeinnlevering_Del_2/src/test/resources/testfiles/TestfileTwo.csv";
        File file = new File(path);
        PatientRegister patients = new PatientRegister();
        FileReader fileReader = null;
        BufferedReader reader = null;
        try {
            patients.addPatient(new Patient("12345678909", "Kapa",
                    "Napa", "Tumor", "Dr. Granskauen"));

            fileReader = new FileReader(file);
            reader = new BufferedReader(fileReader);

            writer.writeToCSV(patients, file);
            message = reader.readLine();
        } catch (AddException | Exception exception) {
            System.out.println(exception.getMessage());
        } finally {
            assertEquals("Kapa;Napa;Dr. Granskauen;12345678909;Tumor", message);
        }
    }

    @Test
    public void successfullyHandlesNotCSVFile() {
        CSVWriter writer = new CSVWriter();
        String message = "";
        String path = "/Users/Farstad/OneDrive/Mappeinnlevering_Del_2/src/main/resources/student/ntnu/idatt2001/magnusfarstad/images/addNew.png";
        File file = new File(path);
        PatientRegister patients = new PatientRegister();

        try {
            patients.addPatient(new Patient("73950291856", "Jonni",
                    "Cash", "Double trouble", "Dr. Granskauen"));


            writer.writeToCSV(patients, file);

        } catch (Exception | AddException exception) {
            System.out.println(exception.getMessage());
            message = exception.getMessage();
        } finally {
            assertEquals("Unsupported file", message);
        }
    }
}
