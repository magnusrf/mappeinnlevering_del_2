module student.ntnu.idatt2001.magnusfarstad {
    requires javafx.controls;
    requires javafx.fxml;

    opens student.ntnu.idatt2001.magnusfarstad to javafx.fxml;
    opens student.ntnu.idatt2001.magnusfarstad.model to javafx.base;
    opens student.ntnu.idatt2001.magnusfarstad.controller to javafx.fxml;
    
    exports student.ntnu.idatt2001.magnusfarstad;
}