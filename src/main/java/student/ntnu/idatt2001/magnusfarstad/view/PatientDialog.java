package student.ntnu.idatt2001.magnusfarstad.view;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import student.ntnu.idatt2001.magnusfarstad.model.Patient;

/**
 * Class representing a custom dialog for adding or
 * editing a patient.
 */
public class PatientDialog extends Dialog<Patient> {

    public enum Mode {
        NEW, EDIT
    }

    private final Mode mode;

    private Patient existingPatient = null;

    /**
     * Constructor when a patient will be added.
     */
    public PatientDialog() {
        super();
        this.mode = Mode.NEW;

        createPatient();
    }

    /**
     * Constructor when a patient will be edited.
     * @param patient The patient to be edited.
     */
    public PatientDialog(Patient patient) {
        super();
        this.mode = Mode.EDIT;

        this.existingPatient = patient;
        createPatient();
    }

    /**
     * Helper method for showing a warning dialog.
     * It will be displayed when an invalid patient
     * is to be instantiated, based on the constructor
     * of Patient.
     * @param exceptionMessage The message to be displayed to
     *                         the user.
     */
    private void showInvalidPatientDialog(String exceptionMessage) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText("Invalid patient");
        alert.setContentText(exceptionMessage);
        System.out.println(exceptionMessage);
        alert.showAndWait();
    }

    /**
     * Method for showing the dialog
     * when a patient is to be added or edited.
     * This is where the dialog is created and displayed.
     *
     * Based on if a patient will be added or edited,
     * it sets the title accordingly.
     * Then the dialog is created.
     *
     * If the constructor for edit is used, this method will edit
     * the patient provided.
     * If the constructor for add is used, this method will try to
     * add the patient provided to the register.
     */
    private void createPatient() {
        switch (this.mode) {
            case NEW:
                setTitle("Patient Details - Add");
                break;

            case EDIT:
                setTitle("Patient Details - Edit");
                break;

            default:
                setTitle("Patient Details - UNKNOWN MODE...");
        }

        // Adding buttons OK and Cancel to the dialog:
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        // Creating and setting the different elements of the dialog:
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField firstName = new TextField();
        TextField lastName = new TextField();
        TextField socialSecurityNumber = new TextField();
        TextField diagnosis = new TextField();
        TextField generalPractitioner = new TextField();

        firstName.setPromptText("First name");
        lastName.setPromptText("Last name");
        socialSecurityNumber.setPromptText("Social security number");
        diagnosis.setPromptText("Diagnosis");
        generalPractitioner.setPromptText("General practitioner");

        grid.add(new Label("First name: "), 0, 0);
        grid.add(new Label("Last name: "), 0, 1);
        grid.add(new Label("Social security number: "), 0, 2);
        grid.add(new Label("Diagnosis: "), 0, 3);
        grid.add(new Label("General practitioner: "), 0, 4);

        grid.add(firstName, 1, 0);
        grid.add(lastName, 1, 1);
        grid.add(socialSecurityNumber, 1, 2);
        grid.add(diagnosis, 1, 3);
        grid.add(generalPractitioner, 1, 4);

        getDialogPane().setContent(grid);

        // If true, the dialog will display the current attributes of the patient:
        if((mode == Mode.EDIT)) {
            firstName.setText(existingPatient.getFirstName());
            lastName.setText(existingPatient.getLastName());
            socialSecurityNumber.setText(existingPatient.getSocialSecurityNumber());
            diagnosis.setText(existingPatient.getDiagnosis());
            generalPractitioner.setText(existingPatient.getGeneralPractitioner());

        }

        /*
        Blocks dialog window from closing when user input is invalid, for all
        cases, except when the user is already in the register.
        Uses exceptions from class Patient to check if new input is valid or not.
        */
        final Button buttonOk = (Button) getDialogPane().lookupButton(ButtonType.OK);
        buttonOk.addEventFilter(ActionEvent.ACTION, event -> {
            try{
                Patient patient = new Patient(socialSecurityNumber.getText(),
                        firstName.getText(),
                        lastName.getText(),
                        diagnosis.getText(),
                        generalPractitioner.getText());
            } catch (Exception exception) {
                event.consume();
                showInvalidPatientDialog(exception.getMessage());
            }
        });

        /*
        Tries to instantiate and add a patient to the register or edit a patient.
        If something goes wrong, it calls the method showInvalidPatientDialog().
         */
        setResultConverter((ButtonType button) -> {
            Patient result = null;
            if(button == ButtonType.OK) {
                try {
                    if(mode == Mode.NEW) {
                        result = new Patient(socialSecurityNumber.getText(),
                                firstName.getText(),
                                lastName.getText(),
                                diagnosis.getText(),
                                generalPractitioner.getText());

                    } else if(mode == Mode.EDIT) {
                        // temporaryPatient is used to check if the edited patient is valid or not.
                        Patient temporaryPatient = new Patient(socialSecurityNumber.getText(),
                                firstName.getText(),
                                lastName.getText(),
                                diagnosis.getText(),
                                generalPractitioner.getText());

                        existingPatient.setFirstName(firstName.getText());
                        existingPatient.setLastName(lastName.getText());
                        existingPatient.setSocialSecurityNumber(socialSecurityNumber.getText());
                        existingPatient.setDiagnosis(diagnosis.getText());
                        existingPatient.setGeneralPractitioner(generalPractitioner.getText());
                        result = existingPatient;
                        }
                    } catch (Exception exception) {
                        showInvalidPatientDialog(exception.getMessage());
                }
            } return result;
        });
    }
}
