package student.ntnu.idatt2001.magnusfarstad.model;

import java.util.Objects;

/**
 * Class representing a Patient.
 * A person must have a first name, last name
 * and a social security number.
 * See constructor for more info.
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Every instance of person must have as follows:
     * @param socialSecurityNumber as number of length 11,
     * @param firstName as text
     * @param lastName as text
     * @param diagnosis is optional and can be both number and/or text
     * @param generalPractitioner is optional, but if present,
     *                            this must be text, not number.
     * @throws NullPointerException if person created does not have
     *                              a first name and/or last name.
     * @throws IllegalArgumentException if social security number is
     *                                  either not present or not of length 11.
     * @throws IllegalArgumentException if either first name, last name
     *                                  or general practitioner is number.
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner) throws Exception{
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;

        if(firstName == "" || lastName == "") {
            throw new NullPointerException("Person must have first name, last name and social security number");
        }

        if(socialSecurityNumber.length() != 11 || !isNumeric(socialSecurityNumber)) {
            throw new IllegalArgumentException("Social security number must be of length 11, numbers only");
        }

        if(isNumeric(firstName) || isNumeric(lastName) || isNumeric(generalPractitioner)) {
            throw new IllegalArgumentException("First name, last name and general practitioner must be text, not numbers");
        }
    }

    /**
     * A method for checking if the argument provided
     * is a number or not. It verifies by trying to
     * parse the argument 'text'. If the argument provided
     * is not a number, the parse.Double will throw
     * a NumberFormatException.
     * @param text The argument to be checked of type String
     * @return {@code true} if the string is a number.
     *         {@code false} if the string is not a number.
     */
    private boolean isNumeric(String text) {
        if(text == null) {
            return false;
        }
        try {
            double number = Double.parseDouble(text);
        } catch (NumberFormatException exception) {
            return false;
        }
        return true;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Overridden method equals.
     * Works as a default equals, but only
     * compares based on social security number
     * as this is a unique number per patient.
     * @param o The object to compare with
     * @return {@code true} if the two objects are the same,
     *         {@code false} if they are not the same.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber=" + socialSecurityNumber +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}
