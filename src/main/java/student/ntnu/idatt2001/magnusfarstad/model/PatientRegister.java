package student.ntnu.idatt2001.magnusfarstad.model;

import student.ntnu.idatt2001.magnusfarstad.exception.AddException;
import student.ntnu.idatt2001.magnusfarstad.exception.RemoveException;
import java.util.ArrayList;

/**
 * This class represent the register where
 * all patients are stored.
 * It only has an ArrayList of patients.
 */
public class PatientRegister {
    private ArrayList<Patient> patients;

    public PatientRegister() {
        this.patients = new ArrayList<>();
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public void setPatients(ArrayList<Patient> patients) {
        this.patients = patients;
    }

    /**
     * Method for adding a patient to the register.
     * If the patient exists in the register already,
     * verified by equals() in Patient, it will not be added.
     * @param patient The patient to be added to the register.
     * @throws AddException if the patient already exists.
     */
    public void addPatient(Patient patient) throws AddException{
        if(patients.stream().anyMatch(p -> p.equals(patient))) {
            throw new AddException("This patient already exists");
        } else {
            patients.add(patient);
        }
    }

    /**
     * Method for deleting a patient from the register.
     * If the patient provided exists, it deletes this
     * patient from the register.
     * @param patient The patient to be deleted.
     * @throws RemoveException if the patient provided
     *                         is null, which means no
     *                         patient is selected.
     */
    public void deletePatient(Patient patient) throws RemoveException{
        if(patient == null) {
            throw new RemoveException("Patient not found");
        } else patients.remove(patient);
    }
}
