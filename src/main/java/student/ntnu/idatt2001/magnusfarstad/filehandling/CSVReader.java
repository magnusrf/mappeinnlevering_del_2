package student.ntnu.idatt2001.magnusfarstad.filehandling;

import student.ntnu.idatt2001.magnusfarstad.exception.AddException;
import student.ntnu.idatt2001.magnusfarstad.model.Patient;
import student.ntnu.idatt2001.magnusfarstad.model.PatientRegister;

import java.io.*;

/**
 * Class for reading a CSV file with one method.
 */
public class CSVReader {

    /**
     * Method that reads every line of the provided file,
     * and adds the Patients in the file, to the register.
     * @param patients The register where the new patients
     *                 will be registered in.
     * @param file The file to be read, and registered in the
     *             PatientRegister.
     * @return The updated PatientRegister, with both old
     *         and new patients.
     * @throws IOException If the file provided is null,
     *                     which means the user canceled the action.
     *                     If the provided file is not a CSV file.
     *                     Or if the file path does not lead to anything.
     */
    public PatientRegister readToCSV(PatientRegister patients, File file) throws IOException{

        // If the user cancels the operation:
        if(file == null) {
            throw new FileNotFoundException("No file imported");
        }

        String path = file.getPath();
        String line = "";
        String[] values = {};
        BufferedReader br = null;

        // If the file is not a CSV file:
        if(!file.getPath().endsWith(".csv")) {
            throw new IOException("The chosen file type is not valid. " +
                    "Please chose another file by clicking on 'Import from .CSV' or " +
                    "cancel the operation");

        } else {
            try (FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader)){

                while((line = bufferedReader.readLine()) != null) {
                    values = line.split(";");
                    try{
                        if(values.length == 4) {
                            patients.addPatient(new Patient(values[3], values[0], values[1], "", values[2]));
                        } else if (values.length == 5) {
                            patients.addPatient(new Patient(values[3], values[0], values[1], values[4], values[2]));
                        }
                    } catch (AddException | Exception exception) {
                        System.out.println(exception.getMessage());
                    }
                }

            } catch (IOException exception) {
                // If the path leads to nothing:
                throw new IOException("The file " + file.getName() + " is not found or does not exist");
            }
        }
        return patients;
    }
}
