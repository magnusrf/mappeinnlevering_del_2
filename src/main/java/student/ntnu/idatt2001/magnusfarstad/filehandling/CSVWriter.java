package student.ntnu.idatt2001.magnusfarstad.filehandling;

import student.ntnu.idatt2001.magnusfarstad.model.PatientRegister;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class for writing to a CSV file with one method.
 */
public class CSVWriter {

    /**
     * Method that writes the patients from the
     * PatientRegister to the file provided.
     * The patients added to the file, are added by
     * separating the attributes of each one with a
     * semicolon, which in turn gets written to the file.
     * @param patients The patients to be written to the file
     * @param file The file to where the patients will be
     *             written to.
     * @throws IOException if either the file is not a CSV file or
     *                     if the file exists, but is a directory rather
     *                     than a file or for any other reason can not
     *                     be written to.
     */
    public void writeToCSV(PatientRegister patients, File file) throws IOException{
        if(!file.getPath().endsWith(".csv")) {
            throw new IOException("Unsupported file");
        }
        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            patients.getPatients()
                    .forEach(patient -> {
                        try {
                            bufferedWriter.append(patient.getFirstName()).append(";")
                                    .append(patient.getLastName()).append(";")
                                    .append(patient.getGeneralPractitioner()).append(";")
                                    .append(patient.getSocialSecurityNumber()).append(";")
                                    .append(patient.getDiagnosis()).append("\n");
                        } catch (IOException exception) {
                            exception.printStackTrace();
                        }
                    });
        } catch (IOException exception) {
            throw new IOException("Could not save to file" + file.getName());
        }
    }
}
