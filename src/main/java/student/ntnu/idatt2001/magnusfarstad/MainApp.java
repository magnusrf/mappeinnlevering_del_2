package student.ntnu.idatt2001.magnusfarstad;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

/**
 * Class representing the application and launches it.
 * The application only has one controller, and therefore
 * it is not necessary to create an instance of it or the
 * PatientRegister here.
 */
public class MainApp extends Application {

    private static Scene scene;

    /**
     * A method that creates the instance of the stage
     * and scene through connecting them to the fxml and controller.
     * It also calls the alertOnExit method when a user tries to close
     * the application through closing the window.
     * @param primaryStage The stage to be created
     * @throws IOException if the loadFXML does not find the
     *                     requested fxml file.
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        scene = new Scene(loadFXML("main"), 640, 480);
        primaryStage.setTitle("Patient Register");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            alertOnExit();
            event.consume();
        });
    }

    /**
     * Method that shows a confirmation dialog,
     * asking if the user wants to close the application.
     * If user presses the OK button, the application closes.
     */
    public static void alertOnExit() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit confirmation");
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setHeaderText("Do you want to close the application?");

        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            Platform.exit();
        }
    }

    /**
     * Helper method to load the requested fxml file.
     * @param fxml The fxml file to be loaded, as a string.
     * @return The loaded fxml for the start method to set as the scene.
     * @throws IOException if the load method does not find the
     *                     requested fxml file.
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApp.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }
}
