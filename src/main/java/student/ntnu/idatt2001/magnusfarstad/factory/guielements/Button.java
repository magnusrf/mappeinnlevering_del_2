package student.ntnu.idatt2001.magnusfarstad.factory.guielements;

import student.ntnu.idatt2001.magnusfarstad.factory.GUIElement;

/**
 * Class representing a Button, can be instantiated by
 * GUIElementFactory
 * It implements GUIElement.
 */
public class Button implements GUIElement {
    @Override
    public void generate() {
        System.out.println("Button generated");
    }
}
