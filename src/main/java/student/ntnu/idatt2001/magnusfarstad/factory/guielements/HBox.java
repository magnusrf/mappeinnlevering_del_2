package student.ntnu.idatt2001.magnusfarstad.factory.guielements;

import student.ntnu.idatt2001.magnusfarstad.factory.GUIElement;

/**
 * Class representing an HBox, can be instantiated by
 * GUIElementFactory
 * It implements GUIElement.
 */
public class HBox implements GUIElement {
    @Override
    public void generate() {
        System.out.println("HBox generated");
    }
}
