package student.ntnu.idatt2001.magnusfarstad.factory;

import student.ntnu.idatt2001.magnusfarstad.factory.guielements.*;

/**
 * Class representing the factory,
 * which instantiates the different
 * GUI elements. Can instantiate whatever
 * GUI element one would like, if a class for it is created.
 */
public class GUIElementFactory {

    /**
     * Method instantiating the different GUI elements.
     * Based on what type of element the client orders,
     * this method instantiates it and returns that object.
     * @param GUIElementType The type of GUI element the client
     *                       orders, and this class will instantiate.
     * @return An instance of the GUI element ordered from the
     *         GUIElementFactoryClient.
     */
    public static GUIElement getGUIElement(String GUIElementType) {
        if(GUIElementType == null) {
            return null;
        }
        if(GUIElementType.equalsIgnoreCase("Button")) {
            return new Button();
        } else if(GUIElementType.equalsIgnoreCase("HBox")) {
            return new HBox();
        } else if(GUIElementType.equalsIgnoreCase("VBox")) {
            return new VBox();
        } else if(GUIElementType.equalsIgnoreCase("Menu")) {
            return new Menu();
        } else if(GUIElementType.equalsIgnoreCase("MenuBar")) {
            return new MenuBar();
        } else if(GUIElementType.equalsIgnoreCase("MenuItem")) {
            return new MenuItem();
        }
        return null;
    }
}
