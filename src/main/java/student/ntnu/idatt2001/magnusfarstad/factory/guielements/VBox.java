package student.ntnu.idatt2001.magnusfarstad.factory.guielements;

import student.ntnu.idatt2001.magnusfarstad.factory.GUIElement;

/**
 * Class representing a VBox, can be instantiated by
 * GUIElementFactory
 * It implements GUIElement.
 */
public class VBox implements GUIElement {
    @Override
    public void generate() {
        System.out.println("VBox generated");
    }
}
