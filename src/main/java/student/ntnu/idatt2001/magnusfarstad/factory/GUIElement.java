package student.ntnu.idatt2001.magnusfarstad.factory;

/**
 * Interface implemented by the different
 * GUI elements.
 */
public interface GUIElement {

    /**
     * Method to 'generate' the different elements.
     */
    public void generate();
}
