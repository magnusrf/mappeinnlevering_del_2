package student.ntnu.idatt2001.magnusfarstad.factory;

/**
 * Main class ordering the different GUI elements
 * from the factory class GUIElementFactory.
 */
public class GUIElementFactoryClient {

    public static void main(String[] args) {

        GUIElement vBox = GUIElementFactory.getGUIElement("VBox");
        vBox.generate();

        GUIElement menuBar = GUIElementFactory.getGUIElement("MenuBar");
        menuBar.generate();

        GUIElement menu = GUIElementFactory.getGUIElement("Menu");
        menu.generate();

        GUIElement menuItem = GUIElementFactory.getGUIElement("MenuItem");
        menuItem.generate();

        GUIElement button = GUIElementFactory.getGUIElement("Button");
        button.generate();

        GUIElement hBox = GUIElementFactory.getGUIElement("HBox");
        hBox.generate();
    }
}
