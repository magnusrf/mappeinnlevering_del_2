package student.ntnu.idatt2001.magnusfarstad.exception;

/**
 * Custom exception class for adding a patient to the register.
 * Takes a string as the exception message, to be displayed
 * if instance of this class is created.
 */
public class AddException extends Throwable{
    private static final long serialVersionUiD = 2L;

    public AddException(String message) {
        super(message);
    }
}
