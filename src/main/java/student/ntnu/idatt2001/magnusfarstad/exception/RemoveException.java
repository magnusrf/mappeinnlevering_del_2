package student.ntnu.idatt2001.magnusfarstad.exception;

/**
 * Custom exception class for deleting a patient from the register.
 * Takes a string as the exception message to be displayed if
 * instance of this class is created.
 */
public class RemoveException extends Throwable{
    private static final long serialVersionUiD = 1L;

    public RemoveException(String message) {
        super(message);
    }
}
