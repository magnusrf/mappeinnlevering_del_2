package student.ntnu.idatt2001.magnusfarstad.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import student.ntnu.idatt2001.magnusfarstad.MainApp;
import student.ntnu.idatt2001.magnusfarstad.exception.AddException;
import student.ntnu.idatt2001.magnusfarstad.exception.RemoveException;
import student.ntnu.idatt2001.magnusfarstad.filehandling.CSVReader;
import student.ntnu.idatt2001.magnusfarstad.filehandling.CSVWriter;
import student.ntnu.idatt2001.magnusfarstad.model.*;
import student.ntnu.idatt2001.magnusfarstad.view.PatientDialog;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The controller for the application.
 * Application uses .fxml file.
 * It only has a patient register and an observable list.
 */
public class MainController implements Initializable {
    private PatientRegister patientRegister;
    private ObservableList<Patient> observablePatientsList;

    @FXML
    private TableView<Patient> patientsTableView;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumberColumn;
    @FXML
    private TableColumn<Patient, String> generalPractitionerColumn;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button editButton;
    @FXML
    private MenuItem addMenuItem;
    @FXML
    private MenuItem editMenuItem;
    @FXML
    private MenuItem deleteMenuItem;
    @FXML
    private MenuItem aboutMenuItem;
    @FXML
    private MenuItem importCSVMenuItem;
    @FXML
    private MenuItem exportCSVMenuItem;
    @FXML
    private MenuItem exitMenuItem;
    @FXML
    private Label statusLabel;


    /**
     * Acts as the constructor for this class.
     * When the app is run with the fxml file, this
     * method is called. It houses all the buttons and
     * fills the table view with the observable list,
     * which is observing the patients from the patient register.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.patientRegister = new PatientRegister();

        createTableColumns();

        /*
        Fills the table view with the patient
        register through observable list
        */
        this.observablePatientsList = FXCollections
                .observableArrayList(this.patientRegister.getPatients());
        this.patientsTableView.setItems(this.observablePatientsList);

        // EventHandlers connecting buttons to methods:
        EventHandler<ActionEvent> addEventHandler = (actionEvent -> {
            try {
                addPatient();
            } catch (AddException exception) {
                System.out.println(exception.getMessage());
            }
        });

        EventHandler<ActionEvent> deleteEventHandler = (actionEvent ->
        {
            try {
                deletePatient(patientsTableView.getSelectionModel().getSelectedItem());
            } catch (RemoveException exception) {
                System.out.println(exception.getMessage());
            }
        });


        EventHandler<ActionEvent> editEventHandler = actionEvent -> {
            try {
                editPatient(patientsTableView.getSelectionModel().getSelectedItem());
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        };

        // Buttons from the application:
        addButton.setOnAction(addEventHandler);
        addMenuItem.setOnAction(addEventHandler);

        deleteButton.setOnAction(deleteEventHandler);
        deleteMenuItem.setOnAction(deleteEventHandler);

        editButton.setOnAction(editEventHandler);
        editMenuItem.setOnAction(editEventHandler);

        aboutMenuItem.setOnAction(actionEvent -> showAbout());
        exitMenuItem.setOnAction(actionEvent -> exit());

        importCSVMenuItem.setOnAction(actionEvent -> loadCSVFile());
        exportCSVMenuItem.setOnAction(actionEvent -> exportToCSV());
    }

    /**
     * Helper method for creating the table columns.
     * It sets the property value for each column to
     * house all attributes of Patient except diagnosis.
     */
    private void createTableColumns() {
        // Create columns:
        this.firstNameColumn
                .setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn
                .setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.socialSecurityNumberColumn
                .setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        this.generalPractitionerColumn
                .setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
    }

    /**
     * Method for updating the observable list,
     * called when the patient register has changed.
     */
    private void updateObservableList() {
        this.observablePatientsList.setAll(this.patientRegister.getPatients());
    }

    /**
     * Method for loading the read CSV file
     * and setting it as the register. It calls
     * the readToCSV() method from the class CSVReader
     * to obtain a new patient register, then sets
     * this register to be the new. Patients already
     * in the previous register will also be transferred
     * to the new one.
     * If the file to be read is not a CSV file, a dialog
     * will tell the user to choose another or cancel.
     *
     * Is called by menu item importCSVMenuItem, through an EventHandler.
     */
    @FXML
    public void loadCSVFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open File");
        File selectedFile = fileChooser.showOpenDialog(new Stage());
        CSVReader reader = new CSVReader();


        try {
            PatientRegister newPatients = reader.readToCSV(patientRegister, selectedFile);

            statusLabel.setText("Import successful");
            patientRegister.setPatients(newPatients.getPatients());
            updateObservableList();

        } catch (IOException exception) {
            if(exception instanceof FileNotFoundException) {
                System.out.println(exception.getMessage());
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setHeaderText("Invalid file type");
                alert.setContentText(exception.getMessage());
                System.out.println(exception.getMessage());

                Optional<ButtonType> result = alert.showAndWait();
                if(result.get() == ButtonType.OK) {
                    loadCSVFile();
                }
            }
        }
    }

    /**
     * Method for calling the writeToCSV() method
     * in class CSVWriter to export a CSV file.
     * If the file has the same name as an existing file
     * the user can choose if they want to overwrite or not.
     * If user cancels, system will print the message "No file exported"
     *
     * Is called by menu item exportCSVMenuItem, through an EventHandler.
     */
    @FXML
    public void exportToCSV() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(".csv",
                "*.csv"));
        File selectedFile = fileChooser.showSaveDialog(new Stage());
        CSVWriter csvWriter = new CSVWriter();

        try {
            if(!selectedFile.createNewFile()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Do you want to overwrite?");
                alert.setContentText("A file with the same name exists. " +
                        "Do you want to overwrite it?");

                Optional<ButtonType> result = alert.showAndWait();
                if(result.get() == ButtonType.OK) {
                    csvWriter.writeToCSV(patientRegister, selectedFile);
                    statusLabel.setText("Export successful");
                } else if(result.get() == ButtonType.CLOSE){
                    exportToCSV();
                }
            } else {
                csvWriter.writeToCSV(patientRegister, selectedFile);
                statusLabel.setText("Export successful");
            }
        } catch (Exception exception) {
            if(exception instanceof NullPointerException) {
                System.out.println("No file exported");
            } else {
                System.out.println(exception.getMessage());
            }
        }
    }

    /**
     * Method for calling the addPatient() method in PatientRegister
     * and creating dialogs accordingly.
     * Uses the dialog class PatientDialog to get the dialog for creating
     * and adding a patient.
     *
     * Is called by button addButton and menu item addMenuItem,
     * through an EventHandler.
     * @throws AddException if addPatient() in PatientRegister
     *                      throws an AddException. This happens
     *                      if the patient already exists.
     * If the patient already exists, the dialog will disappear,
     * to indicate that the action the user tried to do is already done.
     * That be, the patient is already registered. This is because
     * it is unlikely that the user tried to add another patient, but
     * added one already existing, because of coincidence.
     */
    @FXML
    public void addPatient() throws AddException {
        PatientDialog patientDialog = new PatientDialog();
        Optional<Patient> result = patientDialog.showAndWait();

        if(result.isPresent()) {
            try {
                patientRegister.addPatient(result.get());
                statusLabel.setText("Add successful");

            } catch (AddException exception) {
                showPatientNotAddedDialog(exception.getMessage());
                throw new AddException(exception.getMessage());
            }
            this.updateObservableList();
        }
    }

    /**
     * Method for editing the patient.
     * Uses the PatientDialog for editing a patient.
     * Is called by the button editButton and menu item editMenuItem,
     * through an EventHandler.
     * @param patient The patient to be edited.
     * @throws NullPointerException if the patient is null,
     *                              which means no patient
     *                              is selected. This also
     *                              calls the method for showing
     *                              user no patient is selected.
     */
    @FXML
    public void editPatient(Patient patient) throws NullPointerException{
        if(patient == null) {
            showWarningDialog("Edit unsuccessful. No patient selected." +
                    " Please choose a patient");

            throw new NullPointerException("No patient selected");
        } else {
            PatientDialog patientDialog = new PatientDialog(patient);
            Optional<Patient> result = patientDialog.showAndWait();

            if(result.isPresent()) {
                Patient newPatient = result.get();
                //patientRegister.editPatient(newPatient);
                this.updateObservableList();
            }
        }
    }

    /**
     * Method for calling the delete() method in PatientRegister.
     * When trying to delete, the user will get a dialog asking
     * if they are sure they want to delete the selected patient.
     * If unsuccessful, the showWarningDialog() method is called.
     * Is called by the button deleteButton and menu item deleteMenuItem,
     * through an EventHandler.
     * @param patient The patient to be deleted.
     * @throws RemoveException if the deletePatient() in PatientRegister
     *                         throws a RemoveException. This happen if
     *                         no patient is selected.
     */
    @FXML
    public void deletePatient(Patient patient) throws RemoveException{

        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.setTitle("Information");
            alert.setHeaderText("Confirmation");
            alert.setContentText("Are you sure you want to delete this patient?");

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK) {
                this.patientRegister.deletePatient(patient);
                this.updateObservableList();
            }
        } catch (RemoveException exception) {
            showWarningDialog(exception.getMessage());
            throw new RemoveException("Patient not found");
        }
    }

    /**
     * Helper method for showing that an action was
     * unsuccessful. It is called by editPatient()
     * and deletePatient().
     * @param message The message to be displayed
     *                to the user, provided by the
     *                two methods mentioned above.
     */
    @FXML
    private void showWarningDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Information");
        alert.setHeaderText("Action unsuccessful");
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Helper method for showing user that
     * adding the patient was unsuccessful.
     * Method is called by addPatient().
     * @param exception The message to be
     *                  displayed to the user,
     *                  provided by addPatient().
     */
    @FXML
    private void showPatientNotAddedDialog(String exception) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText("Patient was not added");
        alert.setContentText(exception);
        alert.showAndWait();
    }

    /**
     * Method for showing the about dialog.
     * Contains some various information.
     * Is called by the menu item aboutMenuItem,
     * through an EventHandler.
     */
    @FXML
    public void showAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patients Register \n"+ "v0.1-SNAPSHOT");
        alert.setContentText("A marvelous application created by \n" +
                "Magnus Rosvold Farstad \n2021-04-30");
        alert.showAndWait();
    }

    /**
     * Method that calls the exit method in the main class,
     * to close the application.
     * Is called by the menu item exitMenuItem,
     * through an EventHandler.
     */
    @FXML
    public void exit() {
        MainApp.alertOnExit();
    }
}
